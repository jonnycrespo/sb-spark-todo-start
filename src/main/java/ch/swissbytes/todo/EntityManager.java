package ch.swissbytes.todo;

import ch.swissbytes.todo.model.ToDoItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EntityManager {
    private List<ToDoItem> items;

    public EntityManager() {
        items = new ArrayList<>();
    }

    public List<ToDoItem> getItems() {
        return Collections.unmodifiableList(items);
    }

    private int getNextId() {
        int id = 0;
        for (ToDoItem item : items) {
            if (id < item.getId()) {
                id = item.getId();
            }
        }

        return id + 1;
    }

    public ToDoItem addNewItem(String title, String description) {
        ToDoItem item = new ToDoItem(getNextId(), title, description);
        items.add(item);
        return item;
    }
}
